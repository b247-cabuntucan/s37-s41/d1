const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

const app = express();

const port = 4004;

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://carlo:fNgKw6aWKB1b3jH7@zuitt-bootcamp.nk8pfwk.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// CORS stands for Cross-Origin Resources Sharing. 
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(port, () => console.log(`API is now online on port ${port}`));